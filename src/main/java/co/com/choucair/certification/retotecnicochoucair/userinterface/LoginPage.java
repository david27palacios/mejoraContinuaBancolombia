package co.com.choucair.certification.retotecnicochoucair.userinterface;


import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;



public class LoginPage {
    public static final Target TEXT_EMAIL = Target.the("span that show the email").located(By.xpath("//*[@id=\"kc-form-wrapper\"]/div/section/div[1]/span"));
    public static final Target BUTTON_LOGIN = Target.the("button that showing the form by login in the system").located(By.xpath("/html/body/ui-view/unauthenticated-container/div/div/unauthenticated-header/div/div[3]/ul[2]/li[1]/a"));
    public static final Target INPUT_EMAIL = Target.the("INPUT where we write our username").located(By.id("username"));
    public static final Target INPUT_PASSWORD = Target.the("INPUT where we write our password").located(By.id("password"));
    public static final Target BUTTON_SUMBIT = Target.the("button by submit the gorm").located(By.id("kc-login"));
}





