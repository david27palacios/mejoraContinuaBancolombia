package co.com.choucair.certification.retotecnicochoucair.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import co.com.choucair.certification.retotecnicochoucair.userinterface.LoginPage;
import net.serenitybdd.screenplay.questions.TextContent;

public class Answer implements Question<Boolean> {
    private String question;

    public Answer(String question){
        this.question = question;
    }

    public static Answer toThe(String question) {
        return new Answer(question);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result;
        String email= TextContent.of(LoginPage.TEXT_EMAIL).viewedBy(actor).asString();
        if (question.equals(email)){
            result=true;
        }else{
            result=false;
        }
        return result;
    }

}
